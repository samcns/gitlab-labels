gitlab-labels
====

`gitlab-labels`: Simplified GitLab Issue Label management.

Usage
=====

```
Usage:
  ./bin/gitlab-labels init 
  ./bin/gitlab-labels project list 
  ./bin/gitlab-labels project labels <project_name> 
  ./bin/gitlab-labels labels available 
  ./bin/gitlab-labels labels load <package_name> <project_name> 
  ./bin/gitlab-labels labels delete <package_name> <project_name>
```

Examples
========

## Configure `gitlab-labels`

Configuration is a single step that records your GitLab `private-token` in the `gitlab-labels` config file (~/.gitlab-labels/config).

```
./bin/gitlab-labels init

Please enter GitLab private token: 1234567890123
```
## List GitLab projects

```
./bin/gitlab-labels project list
```

## List GitLab project's labels

```
./bin/gitlab-labels project labels webapp1

CLA: Signed
CLA: Unsigned
Priority: Critical
Priority: High
Priority: Low
Priority: Medium
Status: Abandoned
Status: Accepted
Status: Available
...
...
...
```

## List available labels

List all labels available in the `packages/` directory. These are `JSON` files containing `name` and `color` strings. Each file is considered a separate package.

```
./bin/gitlab-labels labels available 
[status]
Status: Abandoned (#000000)
Status: Accepted (#009800)
Status: Available (#bfe5bf)
Status: Blocked (#e11d21)
Status: Completed (#006b75)
Status: In Progress (#cccccc)
Status: On Hold (#e11d21)
Status: Pending (#fef2c0)
Status: Review Needed (#fbca04)
Status: Revision Needed (#e11d21)

[type]
Type: Bug (#e11d21)
Type: Maintenance (#fbca04)
Type: Enhancement (#84b6eb)
Type: Question (#cc317c)

[default]
bug (#fc2929)
duplicate (#cccccc)
enhancement (#84b6eb)
help wanted (#159818)
invalid (#e6e6e6)
question (#cc317c)
wontfix (#ffffff)

[cla]
CLA: Signed (#009800)
CLA: Unsigned (#e11d21)

[priority]
Priority: Low (#009800)
Priority: Medium (#fbca04)
Priority: High (#eb6420)
Priority: Critical (#e11d21)
```

## Load package labels into a project

Load all labels for a given package into a GitLab project.

```
# Load all labels from the `status` package
# into project webapp1.

./bin/gitlab-labels labels load status webapp1
```

## Delete all package labels a project

Delete all labels in a package from a GitLab project.

```
# Delete all labels found in the `status`
# package from the webapp1 project.

./bin/gitlab-labels labels delete status webapp1
```

Installation
============

Clone the repository to your local system:

```
git clone https://gitlab.com/samcns/gitlab-labels.git
cd gitlab-labels/
panda install .
```

Todo
====

* Add tests
* Add custom package / label support

Requirements
============

* [Perl 6](http://perl6.org/)

Acknowledgments
===============

The label package concept and base `.json` packages were sourced from https://github.com/jasonbellamy/git-label-packages.

AUTHORS
=======

  * Sam Morrison

COPYRIGHT AND LICENSE
=====================

Copyright 2016 Sam Morrison

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

