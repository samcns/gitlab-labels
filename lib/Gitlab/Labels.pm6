#!/usr/bin/env perl6

use v6;
use Config::INI;
use Config::INI::Writer;
use HTTP::Tinyish;
use JSON::Tiny;

unit module Gitlab::Labels;

# Globals
my $config_path = $*HOME ~ "/.gitlab-labels";
my $config_file = $config_path ~ "/config";
my $package_dir = $*CWD ~ "/packages";
my $api_url = "https://gitlab.com/api/v3/";

sub path-exists(Str $path, Str $type) {
  if $type ~~ 'f' { return $path.IO ~~ :f }
  if $type ~~ 'd' { return $path.IO ~~ :d }
}

sub load-config(Str $config_file) {
  return Config::INI::parse_file($config_file);
}

sub http-request(Str $path, Str $mode, Str $data?) {

  if (!path-exists($config_file, 'f')) { return  say "Config file does not exist. Please see `gitlab-labels init`" }
  my %conf = load-config($config_file);
  my $http = HTTP::Tinyish.new(agent => "gitlab-labels/0.1.0");
  my $url = $api_url ~ $path;
  my %headers = "PRIVATE-TOKEN" => %conf<gitlab><private_token>;
  my %res;

  %res = $http.get( $url, headers => %headers ) if $mode ~~ 'GET';
  %res = $http.post( $url, headers => %headers, content => "$data" ) if $mode ~~ 'POST';
  %res = $http.delete( $url, headers => %headers, content => "$data" ) if $mode ~~ 'DELETE';

  return %res<content>.defined ?? from-json %res<content> !! [];
}

sub get-projects() {
  return http-request("projects/", "GET");
}

sub get-project-id(Str $project_name) {
  my @projects = get-projects();
  for @projects -> $project {
    return $project<id> if $project<name> ~~ $project_name;
  }
}

sub get-labels(Int $project_id) {
  return http-request("projects/$project_id/labels", "GET");
}

sub load-label(Int $project_id, Str $name, Str $color) {
  return http-request("projects/$project_id/labels", "POST", "name=$name&color=$color");
}

sub load-package-labels (Int $project_id, Str $package_path) {
  my @labels = from-json slurp $package_path;
  my Promise @p = (for @labels -> $label {
      start {
          say "Loading $label<name>: $label<color>";
          load-label $project_id, $label<name>, $label<color>;
      }
      sleep 1;
  });
  await @p if @p ~~ Promise;
}

sub delete-label(Int $project_id, Str $name) {
  return http-request("projects/$project_id/labels", "DELETE", "name=$name");
}

sub delete-package-labels (Int $project_id, $package_path) {
  my @labels = from-json slurp $package_path;
  my Promise @p = (for @labels -> $label {
      start {
          say "Deleting $label<name>: $label<color>";
          delete-label $project_id, $label<name>;
      }
      sleep 1;
  });
  await @p if @p ~~ Promise;
}

# Create config directory / file and store
# Gitlab private token
our sub init(Str :$private_token) {

  # Create config entry
  my %conf;
  if path-exists($config_file, 'f') {
    say "Reading from config...";
    %conf = load-config($config_file);
  } else {
    say "Creating config...";
    if !path-exists($config_path, 'd') { mkdir $config_path }
  }

  %conf<gitlab><private_token> = $private_token;
  Config::INI::Writer::dumpfile(%conf, $config_file);
  # Set permissions (Config::INI::Writer kills 'em)
  chmod 0o600, $config_file;
}

# List all Gitlab projects
our sub project-list {
  return get-projects();
}

# Return a Gitlab project's labels
our sub project-labels(Str :$project_name) {
  my $project_id = get-project-id($project_name);
  return get-labels($project_id);
}

# Return all labels from packages/*.json
our sub labels-available {
  my %packages;
  for dir( "$package_dir/") -> $package { 
    my $package_name = $package.split('/').tail.split('.').head;
    if my @labels = from-json $package.slurp() {
      %packages{$package_name} = @labels;
    }
  }
  return %packages;
}

# Load all package labels to Gitlab project
our sub labels-load(Str :$package_name, Str :$project_name) {

  my $package_path = "$package_dir/$package_name.json";
  if ($package_name !~~ 'all' && !path-exists $package_path, 'f') {
    return say("Package [$package_path] does not exist.");
  }

  my $project_id = get-project-id($project_name);
  
  if $package_name ~~ 'all' {
    say "Importing all package labels...";
    my @packages = "$package_dir".IO.dir( test => /json/);
    for @packages -> $package_name { load-package-labels $project_id, $package_name.Str }
  } else {
    say "Importing package [$package_name] labels...";
    load-package-labels $project_id, $package_path;
  }
  say "Labels imported successfully";
}

# Delete package labels from Gitlab project
our sub labels-delete(Str :$package_name, Str :$project_name) {

  my $package_path = "$package_dir/$package_name.json";
  if ($package_name !~~ 'all' && !path-exists $package_path, 'f') {
    return say("Package [$package_path] does not exist.");
  }

  my $project_id = get-project-id($project_name);

  if $package_name ~~ 'all' {
    say "Deleting all package labels...";
    my @packages = "$package_dir".IO.dir( test => /json/);
    for @packages -> $package_name { delete-package-labels $project_id, $package_name.Str }
  } else {
    say "Deleting package [$package_name] labels...";
    delete-package-labels $project_id, $package_path;
  }
  say "Labels deleted successfully";
}

# Override config defaults
our sub config(Str :$config_dir) {

  if $config_dir.defined {
    my $config_path = IO::Path.new($config_dir.subst('~', $*HOME)).Str; 
    if !path-exists($config_path, 'd') { mkdir $config_path }
    $config_file = $config_dir ~ "/config";
  }

};

# vim: filetype=perl6
